<?php

/**
 * @file
 * Contains podcast_feed view.
 */

$view = new view();
$view->name = 'podcast_feed';
$view->description = 'A view to emulate Drupal core\'s handling of taxonomy/term.';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Podcast feed';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '25';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['row_class'] = 'no-border';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'field_podcast_speaker' => 'field_podcast_speaker',
  'created' => 'created',
  'field_podcast_duration' => 'field_podcast_duration',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_podcast_speaker' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 0,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_podcast_duration' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: View area */
$handler->display->display_options['header']['view']['id'] = 'view';
$handler->display->display_options['header']['view']['table'] = 'views';
$handler->display->display_options['header']['view']['field'] = 'view';
$handler->display->display_options['header']['view']['empty'] = TRUE;
$handler->display->display_options['header']['view']['view_to_insert'] = 'podcast_listing:block_heading';
$handler->display->display_options['header']['view']['inherit_arguments'] = TRUE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
/* Field: Content: Speaker */
$handler->display->display_options['fields']['field_podcast_speaker']['id'] = 'field_podcast_speaker';
$handler->display->display_options['fields']['field_podcast_speaker']['table'] = 'field_data_field_podcast_speaker';
$handler->display->display_options['fields']['field_podcast_speaker']['field'] = 'field_podcast_speaker';
/* Field: Content: Date */
$handler->display->display_options['fields']['field_podcast_date']['id'] = 'field_podcast_date';
$handler->display->display_options['fields']['field_podcast_date']['table'] = 'field_data_field_podcast_date';
$handler->display->display_options['fields']['field_podcast_date']['field'] = 'field_podcast_date';
$handler->display->display_options['fields']['field_podcast_date']['settings'] = array(
  'format_type' => 'date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Duration */
$handler->display->display_options['fields']['field_podcast_duration']['id'] = 'field_podcast_duration';
$handler->display->display_options['fields']['field_podcast_duration']['table'] = 'field_data_field_podcast_duration';
$handler->display->display_options['fields']['field_podcast_duration']['field'] = 'field_podcast_duration';
/* Sort criterion: Content: Date (field_podcast_date) */
$handler->display->display_options['sorts']['field_podcast_date_value']['id'] = 'field_podcast_date_value';
$handler->display->display_options['sorts']['field_podcast_date_value']['table'] = 'field_data_field_podcast_date';
$handler->display->display_options['sorts']['field_podcast_date_value']['field'] = 'field_podcast_date_value';
$handler->display->display_options['sorts']['field_podcast_date_value']['order'] = 'DESC';
/* Contextual filter: Content: Has taxonomy term ID (with depth) */
$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_action'] = 'empty';
$handler->display->display_options['arguments']['term_node_tid_depth']['exception']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['term_node_tid_depth']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['term_node_tid_depth']['title'] = '%1';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['term_node_tid_depth']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['term_node_tid_depth']['validate']['type'] = 'taxonomy_term';
$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '0';
$handler->display->display_options['arguments']['term_node_tid_depth']['break_phrase'] = TRUE;
/* Contextual filter: Content: Has taxonomy term ID depth modifier */
$handler->display->display_options['arguments']['term_node_tid_depth_modifier']['id'] = 'term_node_tid_depth_modifier';
$handler->display->display_options['arguments']['term_node_tid_depth_modifier']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth_modifier']['field'] = 'term_node_tid_depth_modifier';
$handler->display->display_options['arguments']['term_node_tid_depth_modifier']['exception']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['term_node_tid_depth_modifier']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['term_node_tid_depth_modifier']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth_modifier']['specify_validation'] = TRUE;
/* Filter criterion: Content: Published status or admin user */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 0;
$handler->display->display_options['filters']['status_extra']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'podcast' => 'podcast',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status or admin user */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 1;
$handler->display->display_options['filters']['status_extra']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'podcast' => 'podcast',
  'nopodcast' => 'nopodcast',
);
$handler->display->display_options['filters']['type']['group'] = 1;
$handler->display->display_options['path'] = 'taxonomy/term/%';

/* Display: Feed */
$handler = $view->new_display('feed', 'Feed', 'feed');
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'rss_fields';
$handler->display->display_options['style_options']['channel'] = array(
  'core' => array(
    'views_rss_core' => array(
      'description' => 'Messages from Cornerstone Church of Highland, Michigan.',
      'language' => 'en-US',
      'category' => '',
      'image' => 'https://listen.cornerstonehighland.com/sites/podcast/files/cc-logo-square.jpg',
      'copyright' => '',
      'managingEditor' => '',
      'webMaster' => '',
      'generator' => '',
      'docs' => '',
      'cloud' => '',
      'ttl' => '',
      'skipHours' => '',
      'skipDays' => '',
    ),
  ),
);
$handler->display->display_options['style_options']['item'] = array(
  'core' => array(
    'views_rss_core' => array(
      'title' => 'title',
      'link' => 'path',
      'description' => 'body',
      'author' => '',
      'category' => '',
      'comments' => '',
      'enclosure' => 'field_audio_file',
      'guid' => 'uuid',
      'pubDate' => 'field_podcast_date',
    ),
  ),
  'media' => array(
    'views_rss_media' => array(
      'content' => '',
      'title' => '',
      'description' => '',
      'keywords' => '',
      'thumbnail' => '',
      'category' => '',
    ),
  ),
);
$handler->display->display_options['style_options']['feed_settings'] = array(
  'absolute_paths' => 1,
  'feed_in_links' => 0,
);
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Audio File */
$handler->display->display_options['fields']['field_audio_file']['id'] = 'field_audio_file';
$handler->display->display_options['fields']['field_audio_file']['table'] = 'field_data_field_audio_file';
$handler->display->display_options['fields']['field_audio_file']['field'] = 'field_audio_file';
$handler->display->display_options['fields']['field_audio_file']['label'] = '';
$handler->display->display_options['fields']['field_audio_file']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_audio_file']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_audio_file']['type'] = 'enclosure';
/* Field: Content: Date */
$handler->display->display_options['fields']['field_podcast_date']['id'] = 'field_podcast_date';
$handler->display->display_options['fields']['field_podcast_date']['table'] = 'field_data_field_podcast_date';
$handler->display->display_options['fields']['field_podcast_date']['field'] = 'field_podcast_date';
$handler->display->display_options['fields']['field_podcast_date']['label'] = '';
$handler->display->display_options['fields']['field_podcast_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_podcast_date']['settings'] = array(
  'format_type' => 'custom',
  'custom_date_format' => 'r',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Speaker */
$handler->display->display_options['fields']['field_podcast_speaker']['id'] = 'field_podcast_speaker';
$handler->display->display_options['fields']['field_podcast_speaker']['table'] = 'field_data_field_podcast_speaker';
$handler->display->display_options['fields']['field_podcast_speaker']['field'] = 'field_podcast_speaker';
$handler->display->display_options['fields']['field_podcast_speaker']['label'] = '';
$handler->display->display_options['fields']['field_podcast_speaker']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_podcast_speaker']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_podcast_speaker']['type'] = 'text_plain';
/* Field: Content: Scripture */
$handler->display->display_options['fields']['field_podcast_scripture']['id'] = 'field_podcast_scripture';
$handler->display->display_options['fields']['field_podcast_scripture']['table'] = 'field_data_field_podcast_scripture';
$handler->display->display_options['fields']['field_podcast_scripture']['field'] = 'field_podcast_scripture';
$handler->display->display_options['fields']['field_podcast_scripture']['label'] = '';
$handler->display->display_options['fields']['field_podcast_scripture']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_podcast_scripture']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_podcast_scripture']['alter']['text'] = '[field_podcast_speaker], [field_podcast_scripture]';
$handler->display->display_options['fields']['field_podcast_scripture']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_podcast_scripture']['empty'] = '[field_podcast_speaker]';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['body']['alter']['text'] = '[field_podcast_scripture]
[body]';
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['empty'] = '[field_podcast_scripture]';
/* Field: Content: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['label'] = '';
$handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['path']['absolute'] = TRUE;
/* Field: Content: Node UUID */
$handler->display->display_options['fields']['uuid']['id'] = 'uuid';
$handler->display->display_options['fields']['uuid']['table'] = 'node';
$handler->display->display_options['fields']['uuid']['field'] = 'uuid';
$handler->display->display_options['fields']['uuid']['label'] = '';
$handler->display->display_options['fields']['uuid']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status or admin user */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 0;
$handler->display->display_options['filters']['status_extra']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'podcast' => 'podcast',
);
/* Filter criterion: Content: Date (field_podcast_date) */
$handler->display->display_options['filters']['field_podcast_date_value']['id'] = 'field_podcast_date_value';
$handler->display->display_options['filters']['field_podcast_date_value']['table'] = 'field_data_field_podcast_date';
$handler->display->display_options['filters']['field_podcast_date_value']['field'] = 'field_podcast_date_value';
$handler->display->display_options['filters']['field_podcast_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_podcast_date_value']['default_date'] = 'now -4 years';
$handler->display->display_options['path'] = 'taxonomy/term/%/%/feed';
$handler->display->display_options['displays'] = array(
  'default' => 0,
  'page' => 0,
);
