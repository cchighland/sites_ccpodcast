<?php

/**
 * @file
 * Defines custom tokens for this site.
 */

/**
 * Implements hook_token_info().
 */
function mysite_token_info() {
  $info = [];

  // Add any new tokens.
  $info['tokens']['node'] = [
    'field_podcast_term_path' => [
      'name' => t('Podcast Term Path'),
      'description' => t('A version of field_podcast_term suitable for pathauto or breadcrumb use.'),
    ],
    'field_podcast_date_display' => [
      'name' => t('Podcast Date Display'),
      'description' => t('A formatted version of field_podcast_date.'),
    ],
    'field_podcast_date_path' => [
      'name' => t('Podcast Date Path'),
      'description' => t('A version of field_podcast_date suitable for pathauto use.'),
    ],
  ];

  // Return them.
  return $info;
}

/**
 * Implements hook_tokens().
 */
function mysite_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = [];
  if ($type == 'node') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'field_podcast_term_path':
          if (!empty($data['node']->field_podcast_term)) {
            $tid = reset($data['node']->field_podcast_term[LANGUAGE_NONE])['tid'];
            if ($term = taxonomy_term_load($tid)) {
              $replacements[$original] = taxonomy_term_uri($term)['path'];
            }
          }
          break;

        case 'field_podcast_date_display':
          if (!empty($data['node']->field_podcast_date)) {
            $timestamp = reset($data['node']->field_podcast_date[LANGUAGE_NONE]);
            $replacements[$original] = format_date($timestamp['value'], 'custom', 'n/j/Y');
          }
          break;

        case 'field_podcast_date_path':
          if (!empty($data['node']->field_podcast_date)) {
            $timestamp = reset($data['node']->field_podcast_date[LANGUAGE_NONE]);
            $replacements[$original] = format_date($timestamp['value'], 'custom', 'Y-m-d');
          }
          break;
      }
    }
  }
  return $replacements;
}
