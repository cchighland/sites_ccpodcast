<?php

/**
 * Defines the global footer block.
 */
class MySiteFooterBlock extends MyCommonBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('CCListen: Footer message.'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $home = l(
      'Cornerstone Church of Highland &bull; 4995 N Hickory Ridge Rd &bull; Highland, MI 48357 &bull; 248-887-1600',
      'https://www.cornerstonehighland.com/',
      [
        'attributes' => [
          'class' => 'cc-home-link',
          'title' => t('Welcome to Cornerstone. Welcome Home.')
        ],
        'html' => TRUE,
      ]
    );
    $output[] = [
      '#markup' => t('Copyright &copy;!startyear-!year !home All rights reserved', [
        '!startyear' => $this->getStartYear(),
        '!year' => date('Y'),
        '!home' => $home,
      ]),
    ];
  }

  /**
   * Get date of oldest podcast episode.
   */
  private function getStartYear() {
    $result = db_query("SELECT field_podcast_date_value FROM {field_data_field_podcast_date} ORDER BY field_podcast_date_value LIMIT 1")->fetchCol();
    if (!empty($result)) {
      return date('Y', reset($result));
    }
  }

}
